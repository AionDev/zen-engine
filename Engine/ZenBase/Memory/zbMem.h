#pragma once

//=================================================================================================
//! @file		libZenBaseMemory.h
//! @brief		All header files includes for @b LibCore subsection @b Memory.
//! @n@n		See @ref LibCore-Mem module for more infos.
//-------------------------------------------------------------------------------------------------
//!	@addtogroup LibCore-Mem 
//!				Includes memory management modules
//! @section 	LibCore-Mem-Brief More details
//!				This is a component of the @link LibCore Core @endlink library.
//=================================================================================================
#include "zbMemAllocator.h"
